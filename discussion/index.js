// javascript statements;
/*set of statements that will tell 
the machine to perform
*/
console.log("hello once more");

// one line comment <cmd + />
/* mutiline comment <cmd + shift + /> */

// Varibales

/*
	Syntax

		let / const variableName;


*/

let hello;
console.log(hello);


/*console.log(x); //result error
let x;
*/

/*declaration variables*/
let firstName = "Jin";

/*let /const variableName = value;*/
let productName = "desktop computer";
console.log(productName);
let productPrice = 19999;
console.log(productPrice);

const pi = 3.14;
console.log(pi);

/*reasign variable
	variableName = value;

*/
productName = "Laptop";
console.log(productName);

let friend = "kate";
friend = "chance";
console.log(friend); /*frined = chance*/

/*pi = 3.1;
console.log(pi);
*/  
// result error

// reaasigning vs initializing variable
let supplier;
supplier = "Jane Smith Tradings"
console.log(supplier); //results: jane smith trading

supplier = "Zuitt Store";
console.log(supplier); //reassingment of supplier

//var vs let/const
a =5;
console.log(a); //result: 5
var a;

/*b = 6;
console.log(b); //result error
let b;*/

//let / const local / global scope
/*let outerVariable = "hello";
{
	let innerVariable = "hello again";
}
console.log(outerVariable); //result: hello
console.log(innerVariable); //result innervariable not defined*/

//multiple variable declaration
let productCode = 'DC017', productBrand = 'Dell';
console.log(productCode, productBrand);

/*const let = 'hello';
console.log(let); //result error. bound name
*/
//data types
//string
let country = "Philippines";
let city = 'Cebu City'
console.log(city, country); //results: cebu city philippines

//contatenating string
let fullAddress = city + ',' + ' ' + country;
console.log(fullAddress);

let myName = 'Zandro';
let greeting = 'hi i am ' + myName;
console.log(greeting);

//escape character (\)
	// "\n" creates new line

let mailAddress = "Metro Cebu\n\nPhilippines";
console.log(mailAddress);

//single quot inside single quot
let message = 'Zandro\'s property';
console.log(message);

//numbers
//integers and numbers
let headCount = 26;
console.log(headCount);

//decimal number
let grade = 74.9;
console.log(grade);

//exponential notation
let planetDistance = 2e10;
console.log(planetDistance);

//boolean
 let isMarried = false;
 let isSingle = true;
console.log("isMarried " + isMarried);
console.log("isSingle " + isSingle);

//arrays
/*syntax
	let/const arrayName = [1]*/
let grades = [98.1, 98.2, 97.2, 95.1];
console.log(grades);

let anime = ["ddd, ccc, bbb, aaa"]
console.log(anime);

//object data type
/*syntax
	let/cont objectName = { property A : Value A, 
		propertyB : value B
	}*/
let person = {
fullName: "Max Diocampo", 
age: 2, 
isMarried: false,
contact: ["0919000000", "8989 8989"],
address: {
	houseNumber: "002",
	city: "cebu"
	}
}
console.log(person);

//typeof operator

anime[0]= ['one punch man'];
console.log(anime);

//null
let spouse = null;
console.log(spouse);
let zero = 0;
let emptyString = '';

//undefined
let y;
console.log(y);













